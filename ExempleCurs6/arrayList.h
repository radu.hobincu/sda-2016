#ifndef ARRAY_LIST_H
#define ARRAY_LIST_H
 
struct ArrayList {
    short * array;     // pointer la zona de memorie alocată
    unsigned capacity; // dimensiunea zonei alocate
    unsigned size;     // numarul de elemente valide din secventa
};
 

struct ArrayList * createArrayList(unsigned initialCapacity);
 
 

void arrayListMergeSort(struct ArrayList * list);
 
void arrayListQuickSort(struct ArrayList * list);
 

void deleteArrayList(struct ArrayList * list);
 
#endif