/* 
 * File:   main.c
 * Author: rhobincu
 *
 * Created on April 1, 2016, 10:31 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include "arrayList.h"

/*
 * 
 */
int main(int argc, char** argv) {
    struct ArrayList * list = createArrayList(10);
    srand(1234);
    unsigned i;
    for (i = 0; i < 10; i++) {
        list->array[i] = (short) rand();
    }
    list->size = 10;

    for (i = 0; i < list->size; i++) {
        printf("%hd, ", list->array[i]);
    }
    printf("\n");
    arrayListMergeSort(list);

    for (i = 0; i < list->size; i++) {
        printf("%hd, ", list->array[i]);
    }
    printf("\n");
    free(list);
    
    
    list = createArrayList(10);
    srand(1234);
    for (i = 0; i < 10; i++) {
        list->array[i] = (short) rand();
    }
    list->size = 10;

    for (i = 0; i < list->size; i++) {
        printf("%hd, ", list->array[i]);
    }
    printf("\n");
    arrayListQuickSort(list);

    for (i = 0; i < list->size; i++) {
        printf("%hd, ", list->array[i]);
    }
    printf("\n");
    free(list);
    return (EXIT_SUCCESS);
}

