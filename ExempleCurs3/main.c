/* 
 * File:   main.c
 * Author: rhobincu
 *
 * Created on March 11, 2016, 11:28 AM
 */

#include <stdio.h>
#include <stdlib.h>

short * readArray(const char * fileName, unsigned * sizeAddress) {
    FILE * fileDescriptor = fopen(fileName, "r");
    if (fileDescriptor == NULL) {
        return NULL;
    }

    fscanf(fileDescriptor, "%u", sizeAddress);
    short * array = (short *) malloc(*sizeAddress * sizeof (short));
    unsigned i;
    for (i = 0; i<*sizeAddress; i++) {
        fscanf(fileDescriptor, "%hd", &array[i]);
    }

    fclose(fileDescriptor);
    return array;
}

float computeAverage(short * array, unsigned start, unsigned end) {
    if (start == end) return array[start];

    unsigned middle = (end + start) / 2;
    float average1 = computeAverage(array, start, middle);
    float average2 = computeAverage(array, middle + 1, end);
    unsigned size1 = middle - start + 1;
    unsigned size2 = end - (middle + 1) + 1;

    return (size1 * average1 + size2 * average2) / (size1 + size2);
}

/*
 * 
 */
int main(int argc, char** argv) {
    unsigned size;
    short * array = readArray("test_file.txt", &size);

    if (array == NULL) {
        fprintf(stderr, "Nu s-a putut deschide fisierul test_file.txt pentru citire!\n");
        return 1;
    }

    float average = computeAverage(array, 0, size - 1);

    printf("Media este: %.2f\n", average);

    free(array);
    return 0;
}

