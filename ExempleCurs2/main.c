/* 
 * File:   main.c
 * Author: rhobincu
 *
 * Created on March 4, 2016, 11:18 AM
 */

#include <stdio.h>
#include <stdlib.h>

char * readFile(const char * fileName) {
    FILE * fileDescriptor = fopen(fileName, "r");
    if (fileDescriptor == NULL) {
        fprintf(stderr, "File %s could not be opened for reading!\n", fileName);
        return NULL;
    }
    unsigned position = ftell(fileDescriptor);
    printf("Initial position is %u\n", position);
    fseek(fileDescriptor, 0, SEEK_END);
    position = ftell(fileDescriptor);
    printf("Final position (which is also the size) is %u\n", position);
    
    char * buffer = (char*) malloc((position + 1) * sizeof(char));
    if(buffer == NULL){
        fprintf(stderr, "Could not allocate %u bytes of memory!\n", position);
        fclose(fileDescriptor);
        return NULL;
    }
    fseek(fileDescriptor, 0, SEEK_SET);
    fread(buffer, sizeof(char), position, fileDescriptor);
    buffer[position] = '\0';
    fclose(fileDescriptor);
    
    return buffer;
}

/*
 * 
 */
int main(int argc, char** argv) {

    char * fileContents = readFile("/home/rhobincu/test.txt");
    puts(fileContents);
    
    free(fileContents);
    
    return (EXIT_SUCCESS);
}

