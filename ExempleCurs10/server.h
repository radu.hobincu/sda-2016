#ifndef SERVER_H
#define SERVER_H
 
struct Server {
    /* Numele server-ului in retea - sir de caractere */
    char hostname[30]; 
 
    /* Adresa IP a server-ului o secvență de 4 valori numerice între 0 și 255 (ex: 192.168.1.10) */
    unsigned char ipv4[4];
 
    /* Adresa hardware pentru adaptorul de retea - o secventa de 6 bytes, in hexa (ex: 60:57:18:6e:a8:e8). */
    char hardwareAddress[6];
 
    /* Tipul procesorului - sir de caractere. */
    char cpuType[10];
 
    /* Frecventa procesorului in Gigahertz. */
    float cpuFrequencyGhz;
 
    /* Cantitatea de memorie RAM, in Gigabytes. */
    float ramMemoryGigaBytes;
 
    /* Capacitatea discului, in Terabytes. */
    float diskCapacityTeraBytes;
};
 
/**
 * Compara name1 cu name2 si intoarce o valoare corespunzătoare.
 * @param name1 primul nume de comparat.
 * @param name2 al doilea nume de comparat.
 * @return o valoare pozitivă dacă primul nume este mai mare, o 
 *  valoare negativă dacă al doilea nume este mai mare și 0 dacă
 *  numele sunt egale.
 */
int compare(char * name1, char * name2);
 
#endif