#ifndef TREE_MAP_H
#define TREE_MAP_H
 
#include "server.h"
 
struct Pair {
    char * key;
    struct Server value;
};
 
struct TreeNode {
    struct TreeNode * parent;
    struct TreeNode * left;
    struct TreeNode * right;
    struct Pair pair;
};
 
struct TreeMap {
    struct TreeNode * root;
    unsigned size;
};
 
/**
 * Functia aloca memorie si intoarce un pointer la un nou TreeMap.
 * @return adresa noului TreeMap.
 */
struct TreeMap * createTreeMap();
 
/**
 * Functia intoarce numarul de elemente din map.
 * @param map map-ul pentru care se cere dimensiunea.
 * @return numarul de elemente din map.
 */
unsigned treeMapSize(struct TreeMap * map);
 
/**
 * Functia intoarce 1 dacă map-ul nu conține nici un element.
 * @param map map-ul de interes.
 * @return 1 dacă map-ul este gol, 0 în rest.
 */
char treeMapIsEmpty(struct TreeMap * map);
 
/**
 * Functia adauga elementul dat in map. Daca
 *  cheia exista deja, perechea existenta este suprascrisa.
 * @param map map-ul in care trebuie adaugat elementul.
 * @param key cheia din map (numele server-ului)
 * @param value server-ul ce trebuie adaugat.
 */
void treeMapPut(struct TreeMap * map, char * key, struct Server value);
 
/**
 * Functia cauta cheia data în map.
 * @param key cheia de cautat
 * @return 1 daca cheia exista in map, 0 daca nu.
 */
char treeMapHasKey(struct TreeMap * map, char * key);
 
/**
 * Functia cauta o valoarea dupa cheia data în map.
 * @param key cheia de cautat
 * @return valoarea asociată cheii sau o structura goala daca cheia nu exista in map.
 */
struct Server treeMapGet(struct TreeMap * map, char * key);
 
/**
 * Functia elimina perechea cu cheia dată din map daca acesta exista. Daca
 *  nu exista, functia nu are nici un efect.
 * @param map map-ul din care trebuie eliminat elementul.
 * @param key cheia perechii ce trebuie eliminată.
 */
void treeMapRemove(struct TreeMap * map, char * key);
 
/**
 * Functia sterge tree map-ul specificat.
 * @param map tree map-ul ce trebuie sters.
 */
void deleteTreeMap(struct TreeMap * map);
 
#endif