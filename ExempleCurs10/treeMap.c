#include "treeMap.h"
#include "server.h"
#include <stdlib.h>
#include <string.h>


struct TreeNode * createTreeNode(char * key, struct Server value) {
    struct TreeNode * treeNode = (struct TreeNode *) calloc(1, sizeof (struct TreeNode));
    treeNode->pair.key = key;
    treeNode->pair.value = value;
    return treeNode;
}

struct TreeMap * createTreeMap() {
    return (struct TreeMap *) calloc(1, sizeof (struct TreeMap));
}

unsigned treeMapSize(struct TreeMap * map) {
    return map->size;
}

char treeMapIsEmpty(struct TreeMap * map) {
    return map->size == 0;
}

void treeMapPut(struct TreeMap * map, char * key, struct Server value) {
    if (map->size == 0) {
        map->root = createTreeNode(key, value);
        map->size = 1;
        return;
    }

    struct TreeNode * tmpNode = map->root;
    while (1) {
        if (compare(tmpNode->pair.key, key) == 0) {
            tmpNode->pair.key = key;
            tmpNode->pair.value = value;
            return;
        }

        if (compare(tmpNode->pair.key, key) > 0) {
            if (tmpNode->left != NULL) {
                tmpNode = tmpNode->left;
            } else {
                tmpNode->left = createTreeNode(key, value);
                map->size++;
                return;
            }
        }

        if (compare(tmpNode->pair.key, key) < 0) {
            if (tmpNode->right != NULL) {
                tmpNode = tmpNode->right;
            } else {
                tmpNode->right = createTreeNode(key, value);
                map->size++;
                return;
            }
        }
    }
}

char treeMapHasKey(struct TreeMap * map, char * key) {
    struct TreeNode * tmpNode = map->root;
    while (tmpNode != NULL) {
        if (compare(tmpNode->pair.key, key) == 0) {
            return 1;
        }

        if (compare(tmpNode->pair.key, key) > 0) {
            tmpNode = tmpNode -> left;
        }

        if (compare(tmpNode->pair.key, key) < 0) {
            tmpNode = tmpNode -> right;
        }
    }

    return 0;
}

struct Server treeMapGet(struct TreeMap * map, char * key) {
    struct TreeNode * tmpNode = map->root;
    while (tmpNode != NULL) {
        if (compare(tmpNode->pair.key, key) == 0) {
            return tmpNode->pair.value;
        }

        if (compare(tmpNode->pair.key, key) > 0) {
            tmpNode = tmpNode -> left;
        }

        if (compare(tmpNode->pair.key, key) < 0) {
            tmpNode = tmpNode -> right;
        }
    }

    struct Server server;
    memset(&server, 0, sizeof (struct Server));
    return server;
}

void removeNode(struct TreeNode * deadNode, char direction) {
    /* cazul 1, n-are copii */
    if (deadNode->left == NULL && deadNode->right == NULL) {
        if (direction == -1) {
            deadNode->parent->left = NULL;
            free(deadNode);
            return;
        }


        deadNode->parent->right = NULL;
        free(deadNode);
        return;
    }

    /* Cazul 2 - doar un copil*/
    if (deadNode->left == NULL && deadNode->right != NULL) {
        if (direction == -1) {
            deadNode->parent->left = deadNode->right;
            free(deadNode);
            return;
        }


        deadNode->parent->right = deadNode->right;
        free(deadNode);
        return;
    }

    if (deadNode->left != NULL && deadNode->right == NULL) {
        if (direction == -1) {
            deadNode->parent->left = deadNode->left;
            free(deadNode);
            return;
        }

        deadNode->parent->right = deadNode->left;
        free(deadNode);
        return;
    }

    /* cazul 3 - 2 copii*/
    struct TreeNode * tmpNode = deadNode->right;
    direction = 1;
    while (tmpNode->left != NULL) {
        tmpNode = tmpNode->left;
        direction = -1;
    }
    deadNode->pair = tmpNode->pair;
    removeNode(tmpNode, direction);
}

void treeMapRemove(struct TreeMap * map, char * key) {
    if (compare(map->root->pair.key, key) == 0) {
        if (map->size == 1) {
            free(map->root);
            map->root = NULL;
            map->size = 0;
            return;
        }

        if (map->root->left != NULL && map->root->right != NULL) {
            int direction = 1;
            struct TreeNode * tmpNode = map->root->right;
            while (tmpNode->left != NULL) {
                tmpNode = tmpNode->left;
                direction = -1;
            }
            map->root->pair = tmpNode->pair;
            removeNode(tmpNode, direction);
            return;
        }

        if (map->root->left != NULL) {
            struct TreeNode * tmpNode = map->root;
            map->root = tmpNode->left;
            free(tmpNode);
            return;
        }

        if (map->root->right != NULL) {
            struct TreeNode * tmpNode = map->root;
            map->root = tmpNode->right;
            free(tmpNode);
            return;
        }
    }

    struct TreeNode * tmpNode = map->root;
    int direction = 0;
    while (tmpNode != NULL) {
        if (compare(tmpNode->pair.key, key) == 0) {
            removeNode(tmpNode, direction);
        }

        if (compare(tmpNode->pair.key, key) > 0) {
            tmpNode = tmpNode -> left;
            direction = -1;
        }

        if (compare(tmpNode->pair.key, key) < 0) {
            tmpNode = tmpNode -> right;
            direction = 1;
        }
    }
}

void deleteNode(struct TreeNode * node) {
    if (node == NULL) return;

    deleteNode(node->left);
    deleteNode(node->right);
    free(node);
}

void deleteTreeMap(struct TreeMap * map) {
    deleteNode(map->root);
    free(map);
}
